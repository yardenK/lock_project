import face_recognition
import cv2
import numpy as np
import pickle

USERS_PATH = "users.p"
PICTURE_WIDTH = 800
PICTURE_HEIGHT = 1200
ACCURACY_LEVEL = 0.4


class User:
    name = ""
    code = None

    def __init__(self, name, code):
        self.name = name
        self.code = code


class Face:

    users = list()
    username = ''

    def __init__(self):

        # check if the data base exist
        try:

            # load all users
            self.users = pickle.load(open(USERS_PATH, "rb"))

        except FileNotFoundError as e:
            pass

    def check_faces(self, img):

        # resize the image
        new_size = (PICTURE_WIDTH, PICTURE_HEIGHT)
        img = img.resize(new_size)
        open_cv_image = np.array(img)
        rgb_image = open_cv_image[:, :, ::-1]
        final_img = rgb_image

        # encode the new img
        unknown_encoding = face_recognition.face_encodings(final_img)

        # search go over all the faces in the new img
        for new_face in unknown_encoding:

            # search over all the existing users faces
            for known_face in self.users:

                # check the face
                result = face_recognition.compare_faces([known_face.code], new_face, ACCURACY_LEVEL)
                print(result)

                # if face is found quit
                if True in result:
                    return known_face.name

        return None

    def add_face(self, name, img):

        # resize the image
        new_size = (PICTURE_WIDTH, PICTURE_HEIGHT)
        img = img.resize(new_size)
        open_cv_image = np.array(img)
        rgb_image = open_cv_image[:, :, ::-1]
        final_img = rgb_image

        # encode the new face
        img_encoding = face_recognition.face_encodings(final_img)[0]

        # add the new user
        new_user = User(name, img_encoding)
        self.users.append(new_user)

        # save the new user
        pickle.dump(self.users, open(USERS_PATH, "wb"))

        print("end")

