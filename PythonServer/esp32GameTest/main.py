import base64
import socket
import face_recognition
from PIL import Image

import face_ditect
import sqlite3
import time
import io
import threading
import request_handler
import base64
import cv2
import numpy as np

CHECK_FACE_CODE = "1"
ADD_FACE_CODE = "2"
RECOVER_DATA_BYTES_SIZE = 1000000

PORT = 12345
face = face_ditect.Face()


# convert the send picture data to PNG
def get_picture(sock):
    msg = bytearray()

    while True:

        # get all the image data
        data = sock.recv(RECOVER_DATA_BYTES_SIZE)
        msg = msg + data

        # keep waiting for the image
        if str(data) == "b''" and len(msg) == 0:
            continue

        # quit the loop after all the image was sent
        if str(data) == "b''":
            break

    # convert the image text to png
    image1 = io.BytesIO(msg)
    image = Image.open(image1).convert("RGB")

    return image


# convert the message from the client to task
def get_task(sock):
    data = sock.recv(12)
    data = data.decode("utf-8")

    data = data[2:]

    code = data[:1]
    name = data[1:]

    name = name.replace("0", "")

    return code, name


# called when a new client connect
def handel_client(sock):
    print("got client")

    code, name = get_task(sock)
    img = get_picture(sock)

    if code == CHECK_FACE_CODE:
        check_face(img)

    elif code == ADD_FACE_CODE:
        add_face(name, img)

    sock.close()


# add new face to the data base
def add_face(name, img):
    face.add_face(name, img)


# check if face is in the data base
def check_face(img):
    print(face.check_faces(img))

    # TODO : send to the esp32 to open the door


def main():

    # connect the socket
    s = socket.socket()
    s.bind(('', PORT))
    s.listen(5)

    print("ready")

    # wait for clients
    while True:

        c, addres = s.accept()

        x = threading.Thread(target=handel_client, args=(c,))
        x.start()


if __name__ == '__main__':
    main()









