package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;

import android.content.Intent;
import android.content.pm.PackageManager;

import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.net.Uri;

import android.os.Bundle;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;


import static java.lang.System.out;

public class MainActivity extends AppCompatActivity {

    private static final int SERVER_PORT = 12345;
    private static final String SERVER_IP = "192.168.0.108";
    private static final int PICTURE_RESULT = 2;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 100;
    private static final int IMAGE_QUALITY = 50;

    private static final String CHECK_IMAGE_CODE = "1";
    private static final String ADD_IMAGE_CODE = "2";


    private ImageView imageView;
    private TextView textView;
    private Button openCamera;
    private EditText nameInput;

    Thread sendImgThread = null;
    ContentValues values;
    Uri imageUri;
    Bitmap bit;
    Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openCamera = (Button) findViewById(R.id.openCamera);
        imageView = (ImageView) findViewById(R.id.imageView);
        textView = (TextView) findViewById(R.id.textView);
        nameInput = (EditText) findViewById(R.id.nameInput);


        // ask for camera permission if needed
        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
        }

    }

    // called when the user click the button that check if the user is in the database
    public void clickSendCheck(View view)
    {

        String task = CHECK_IMAGE_CODE + "000000000";

        sendImgThread = new Thread(new sendImg(task));
        sendImgThread.start();

    }

    // called when the user click the button that add user to the data base
    public void clickSendAdd(View view)
    {
        String name = nameInput.getText().toString();
        String task = ADD_IMAGE_CODE + name;
        while(task.length() < 10)
            task = task + "0";

        sendImgThread = new Thread(new sendImg(task));
        sendImgThread.start();
    }

    // called when the user click the button that open the camera
    public void clickOpenCamera(View view)
    {

        values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PICTURE_RESULT);


    }

    // called when the camera return a picture
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case PICTURE_RESULT:
                if (requestCode == PICTURE_RESULT)
                    if (resultCode == Activity.RESULT_OK) {
                        try {

                            // save the image
                            bit = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

                            // display it
                            imageView.setImageBitmap(bit);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
        }

    }


    // this function send image and task data to the server
    class sendImg implements Runnable {

        String sendData;
        sendImg(String sendData)
        {
            this.sendData = sendData;
        }

        public void run() {

            try {

                // open the socket
                socket = new Socket(SERVER_IP, SERVER_PORT);
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());

                // send the task data
                out.writeUTF(this.sendData);
                out.flush();

                // send the image
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                bit.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, bout);
                out.write(bout.toByteArray());

                // close the socket
                socket.close();

            } catch (final Exception e) {

                // print the error
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(e.toString());
                    }
                });
            }
        }
    }

}